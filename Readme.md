Quadratic Net
=============

Description
-----------

Given a list of d-dimensional quadratic forms (symmetric real dxd
matrices) Q_i, attempt to find a point x such that x^T Q_i x is
positive for all i.

This is a quadratically constrained program, which is NP hard in
general. Instead, we implement some heuristic methods suggested in the
convex optimization literature.

The search happens in 4 stages:

1. Solve the semidefinite relaxation of the above problem:
  
        find X (positive semidefinite matrix)
        such that Tr(X Q_i) >= 0 for all i

    The quadratically constrained problem is equivalent to its
    semidefinite relaxation, together with an extra constraint that X
    is rank 1, i.e. X = x x^T for some x. The problem is easy to solve
    if we remove the rank-1 condition. We use the high-precision
    solver SDPB.

    If the semidefinite relaxation has no solution, then the original
    problem has no solution. In this case, we can rigorously terminate
    our search.

2. If X is found, attempt to reduce the rank of X using the method
described in [this paper](https://arxiv.org/abs/1609.02609).

3. Randomly sample vectors x from a Gaussian distribution such that
E[x x^T] = X, and test if they are feasible. The advantage of
this distribution is that E[x^T Q_i x] >= 0 for all i because X =
E[x x^T] is a solution to the semidefinite relaxation. If we find
a feasible point, go to step 4. If we do not succeed in finding a
feasible point, we have to stop. This step is non-rigorous, and
we can never be completely sure that we have not missed a
possible feasible point.

4. If we find a feasible x, use Newton's method to flow away from
the boundaries of the feasible region to a point roughly in the
"middle" of the feasible region. We use a potential V(x) =
-\sum_i \log |x^T Q_i x / x^T x| and try to minimize it.

The program contains some options to tweak the above algorith. Run
with --help to see a description.

Installation and Usage
----------------------

Install [haskell stack](https://www.haskellstack.org/).

To build:

    stack build

To find a feasible point, pipe a list of quadratic forms in json format to `stdin`:

    cat tests/hard_feasible.json | stack exec -- quadratic-net-exe \
      --pvm2sdpExecutable PATH --sdpbExecutable PATH --workDir PATH

where the `--pvm2sdpExecutable` and `--sdpbExecutable` arguments are set to the
scripts that execute those programs. See the scripts directory for examples.
`--workDir` specifies the directory that temporary sdpb files will be written to

The result will be written to `stdout` in json format: it will either be `null` if no
feasible point is found, or a vector `[v_1,...,v_d]`. Informational output is
written to `stderr`.
