#!/bin/bash

module load cmake/3.10.2 gcc/7.3.0 openmpi/3.0.0 boost/1_68_0-gcc730
export LD_LIBRARY_PATH=/home/wlandry/elemental/libelemental_bin/lib:$LD_LIBRARY_PATH
export OPENBLAS_NUM_THREADS=1 
/home/wlandry/install/bin/sdpb $@
