{-# LANGUAGE ApplicativeDo       #-}
{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE PolyKinds           #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications    #-}

module Main where

import           Control.Applicative         (optional)
import           Control.Monad.Except        (runExceptT)
import qualified Data.List.NonEmpty          as NE
import qualified Data.Matrix                 as M
import           Data.Monoid                 ((<>))
import           Data.Proxy                  (Proxy (..))
import           Data.Reflection             (reifyNat)
import           Data.Singletons.Prelude.Num
import           Data.Singletons.TypeLits
import qualified Data.Text.IO                as T
import           GHC.IO.Handle               (hDuplicate, hDuplicateTo)
import qualified Linear.V                    as L
import           Numeric.Eigen.Static        (fromRawMatrix)
import           Numeric.Rounded             hiding (precision)
import           Options.Applicative
import           QuadraticNet
import qualified QuadraticNet                as QN
import qualified SDPB.Solver                 as SDPB
import           System.IO                   (IOMode (..), hClose, hPrint,
                                              hPutStrLn, openFile, stderr,
                                              stdout)

defaultConfig :: QuadraticNetConfig
defaultConfig = defaultQuadraticNetConfig undefined undefined undefined

quadraticNetConfigOpts :: Parser (QuadraticNetConfig, Maybe FilePath)
quadraticNetConfigOpts = do
  sdpRelaxationConfig <- sdpRelaxationConfigOpts
  newtonConfig <- newtonConfigOpts
  numSamplePoints <-
    option auto (long "numSamplePoints"
                <> metavar "INT"
                <> value (QN.numSamplePoints defaultConfig)
                <> showDefault
                <> help "Maximum number of random points to sample to find a feasible vector")
  covarianceFuzzing <-
    option auto (long "covarianceFuzzing"
                <> metavar "DOUBLE"
                <> value (QN.covarianceFuzzing defaultConfig)
                <> showDefault
                <> help "Add covarianceFuzzing^2 * identity to the covariance matrix used for \
                        \sampling vectors. Essentially spreads out the search space by \
                        \size covarianceFuzzing. This can be helpful if rank minimization \
                        \works too well so that the initial covariance matrix covers a very \
                        \tiny area almost on a corner of the allowed space.")
  sdpBarrierShift <-
    option auto (long "sdpBarrierShift"
                <> metavar "DOUBLE"
                <> value (QN.sdpBarrierShift defaultConfig)
                <> showDefault
                <> help "When solving the SDP relaxation, require the stronger condition \
                        \Tr(X Q) > sdpBarrierShift. This shrinks the allowed region so \
                        \that a point on the boundary of the shrunken region is in the \
                        \interior of the original region. The tradeoff is that if \
                        \sdpBarrierShift is too large, the allowed region can disappear.")
  precision <-
    option auto (long "precision"
                <> metavar "INT"
                <> value (QN.precision defaultConfig)
                <> showDefault
                <> help "Precision to use for SDP relaxation and Newton search")
  outFile <-
    optional $ strOption (long "outFile"
                          <> short 'o'
                          <> metavar "PATH"
                          <> help "Output final result to this file. Optional. If not specified, \
                                  \stdout will be used.")
  return (QuadraticNetConfig {..}, outFile)

sdpbConfigOpts :: Parser SDPBConfig
sdpbConfigOpts = do
  let defaultSDPBParams = QN.sdpbParams (QN.sdpbConfig (QN.sdpRelaxationConfig defaultConfig))
  pvm2sdpExecutable <-
    strOption (long "pvm2sdpExecutable"
              <> metavar "PATH"
              <> help "Path to the pvm2sdp executable")
  sdpbExecutable <-
    strOption (long "sdpbExecutable"
              <> metavar "PATH"
              <> help "Path to the sdpb executable")
  workDir <-
    strOption (long "workDir"
              <> metavar "PATH"
              <> help "Work directory for sdpb files")
  sdpbPrecision <-
    option auto (long "sdpbPrecision"
                <> metavar "INT"
                <> value (SDPB.precision defaultSDPBParams)
                <> showDefault
                <> help "precision paramter for SDPB. Should be larger than precision \
                        \to avoid cholesky decomposition errors.")
  sdpbInitialMatrixScalePrimal <-
    option auto (long "sdpbInitialMatrixScalePrimal"
                <> metavar "DOUBLE"
                <> value (SDPB.initialMatrixScalePrimal defaultSDPBParams)
                <> showDefault
                <> help "initialMatrixScalePrimal for SDPB.")
  sdpbInitialMatrixScaleDual <-
    option auto (long "sdpbInitialMatrixScaleDual"
                <> metavar "DOUBLE"
                <> value (SDPB.initialMatrixScaleDual defaultSDPBParams)
                <> showDefault
                <> help "initialMatrixScaleDual for SDPB.")
  sdpbMaxComplementarity <-
    option auto (long "sdpbMaxComplementarity"
                <> metavar "DOUBLE"
                <> value (SDPB.maxComplementarity defaultSDPBParams)
                <> showDefault
                <> help "maxComplementarity for SDPB.")
  sdpbDualityGapThreshold <-
    option auto (long "sdpbDualityGapThreshold"
                <> metavar "DOUBLE"
                <> showDefault
                <> value (SDPB.dualityGapThreshold defaultSDPBParams)
                <> help "dualityGapThreshold for SDPB.")
  sdpbStepLengthReduction <-
    option auto (long "sdpbStepLengthReduction"
                <> metavar "DOUBLE"
                <> showDefault
                <> value (SDPB.stepLengthReduction defaultSDPBParams)
                <> help "stepLengthReduction for SDPB.")
  return SDPBConfig
    { sdpbParams = SDPB.defaultParams
      { SDPB.precision                = sdpbPrecision
      , SDPB.initialMatrixScalePrimal = sdpbInitialMatrixScaleDual
      , SDPB.initialMatrixScaleDual   = sdpbInitialMatrixScalePrimal
      , SDPB.maxComplementarity       = sdpbMaxComplementarity
      , SDPB.dualityGapThreshold      = sdpbDualityGapThreshold
      , SDPB.stepLengthReduction      = sdpbStepLengthReduction
      }
    , ..
    }

sdpRelaxationConfigOpts :: Parser SDPRelaxationConfig
sdpRelaxationConfigOpts = do
  let defaultSDPRelaxationConfig = QN.sdpRelaxationConfig defaultConfig
  maxRankMinimizationSteps <-
    option auto (long "maxRankMinimizationSteps"
                <> metavar "INT"
                <> value (QN.maxRankMinimizationSteps defaultSDPRelaxationConfig)
                <> showDefault
                <> help "Number of times to attempt rank minimization for \
                        \the sdp (see https://arxiv.org/abs/1609.02609)")
  eigenvalueReductionThreshold <-
    option auto (long "eigenvalueReductionThreshold"
                <> metavar "DOUBLE"
                <> value (QN.eigenvalueReductionThreshold defaultSDPRelaxationConfig)
                <> showDefault
                <> help "Keep performing rank minimization as long as it decreases \
                        \the largest small eigenvalue by this factor")
  eigenvalueSizeThreshold <-
    option auto (long "eigenvalueSizeThreshold"
                <> metavar "DOUBLE"
                <> value (QN.eigenvalueSizeThreshold defaultSDPRelaxationConfig)
                <> showDefault
                <> help "Stop performing rank minimization if the largest small \
                        \eigenvalue is less than this value")
  sdpbConfig <- sdpbConfigOpts
  return SDPRelaxationConfig {..}

newtonConfigOpts :: Parser NewtonConfig
newtonConfigOpts = do
  let defaultNewtonConfig = QN.newtonConfig defaultConfig
  maxNewtonSteps <-
    option auto (long "maxNewtonSteps"
                <> metavar "INT"
                <> value (QN.maxNewtonSteps defaultNewtonConfig)
                <> showDefault
                <> help "Maximum number of steps for Newton's method")
  newtonSearchThreshold <-
    option auto (long "newtonSearchThreshold"
                <> metavar "DOUBLE"
                <> value (QN.newtonSearchThreshold defaultNewtonConfig)
                <> showDefault
                <> help "Stop Newton search when step size reaches this threshold")
  return NewtonConfig {..}

main :: IO ()
main = do
  (qnConfig, mOutFile) <- execParser opts
  stdout_excl <- hDuplicate stdout
  hDuplicateTo stderr stdout
  reifyPrecision (precision qnConfig) $
    \(_ :: Proxy p) -> do
      qForms :: NE.NonEmpty (M.Matrix (Rounded 'TowardZero p)) <-
        fmap M.fromLists . NE.fromList . read <$> getContents
      result <- reifyNat (fromIntegral (M.nrows (NE.head qForms))) $
        \(_ :: Proxy j) -> withKnownNat (SNat @j %- SNat @1) $ do
          let qForms' = fmap (fromRawMatrix @j @j) qForms
          result' <- runExceptT (runSearch qnConfig qForms')
          return $ fmap (fmap L.toVector) result'
      outHandle <- case mOutFile of
        Nothing      -> return stdout_excl
        Just outFile -> openFile outFile WriteMode
      case result of
        Left e         -> T.putStrLn e
        Right (Just v) -> hPrint outHandle v
        Right Nothing  -> hPutStrLn outHandle "null"
      hClose outHandle
  where
    opts = info (quadraticNetConfigOpts <**> helper)
      ( fullDesc
      <> progDesc "Find a vector (roughly) in the center of a region \
                  \where the given quadratic forms are all positive. \
                  \Reads quadratic forms from stdin.")
