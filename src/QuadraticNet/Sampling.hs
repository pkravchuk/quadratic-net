{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE PolyKinds           #-}
{-# LANGUAGE ScopedTypeVariables #-}

module QuadraticNet.Sampling where

import           Data.Foldable         (all)
import qualified Data.List.NonEmpty    as NE
import qualified Data.Matrix.Static    as M
import           Data.Maybe            (fromMaybe)
import           Fmt
import           GHC.TypeNats          (KnownNat)
import           Linear.V              (V)
import           Numeric.Eigen.Static  (choleskyDecomposition)
import qualified Numeric.LinearAlgebra as LA
import           Numeric.Rounded
import           QuadraticNet.Math     (bilinearPair, bilinearPairH,
                                        fromHVector, matXvec, normalizeV,
                                        toHMatrix)
import           QuadraticNet.Util     (logInfo)
import           System.Random         (randomIO)

samplePoints
  :: forall j r p . (KnownNat j, Rounding r, Precision p)
  => NE.NonEmpty (M.Matrix j j (Rounded r p))
  -> M.Matrix j j (Rounded r p)
  -> Int
  -> IO (Maybe (V j (Rounded r p)))
samplePoints qForms covariance numSamples = do
  logInfo $ "Sampling up to "+||numSamples||+" points with covariance "+||M.toLists covariance||+""
  go numSamples
  where
    l = fromMaybe (error "Covariance matrix is not positive semidefinite")
      (choleskyDecomposition covariance)
    size = M.nrows covariance
    -- We replace each quadratic form Q with Q' = L^T Q L. This means
    -- that we can sample x from an unscaled Gaussian distribution, so
    -- that E[x^T Q' x] = E[(L x)^T Q (L x)] = E[Tr(Q L x x^T L^T)] =
    -- Tr(Q L L^T). This essentially rescales the feasible region in x
    -- to be O(1) in size.
    qForms' = fmap (\q -> toHMatrix (fmap toDouble (M.transpose l * q * l))) qForms
    -- We sample the Gaussian vectors and test their admissibility at
    -- double precision, for performance reasons. Because the feasible
    -- region is rescaled, it should be ok to use low precision here.
    randomSample = fmap (\seed -> LA.randomVector seed LA.Gaussian size) randomIO
    isFeasibleLowPrec x' = all (\q' -> bilinearPairH q' x' > 0) qForms'
    -- Finally, once a sample x is found, we convert it to high
    -- precision and then multiply by L to obtain our final sample
    -- point
    convertSample x' = l `matXvec` fmap realToFrac (fromHVector x')
    isFeasible x = all (\q -> bilinearPair q x > 0) qForms
    go 0 = return Nothing
    go n = do
      x' <- randomSample
      case isFeasibleLowPrec x' of
        False -> go (n-1)
        True -> do
          let x = normalizeV (convertSample x')
          case isFeasible x of
            False -> do
              logInfo "Low precision sample vector failed to be feasible at high precision."
              go (n-1)
            True -> do
              logInfo $ "Sample point "+||numSamples - n + 1||+"/"+||numSamples||+" is feasible."
              return (Just x)
