{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE PolyKinds           #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}

module QuadraticNet.Solver where

import           Numeric.Rounded
import qualified SDPB.Solver           as SDPB
import           SDPB.SDP.Types        (SDP)
import           SDPB.SDP.Write        (writeSDPActionsSimple)
import           System.Directory      (createDirectoryIfMissing, doesFileExist)
import           System.FilePath.Posix ((</>))
import           System.IO.Temp        (withTempDirectory)

data SDPBConfig = SDPBConfig
  { sdpbParams        :: SDPB.Params
  , pvm2sdpExecutable :: FilePath
  , sdpbExecutable    :: FilePath
  , workDir           :: FilePath
  } deriving (Show)

defaultSDPBConfig :: FilePath -> FilePath -> FilePath -> SDPBConfig
defaultSDPBConfig pvm2sdpExecutable sdpbExecutable workDir = SDPBConfig
  { sdpbParams = SDPB.defaultParams
    { SDPB.initialMatrixScalePrimal = 1e8
    , SDPB.initialMatrixScaleDual   = 1e8
    , SDPB.dualityGapThreshold      = 1e-40
    , SDPB.maxComplementarity       = 1e40
    , SDPB.precision                = 384
    }
  , ..
  }

writeSDPIO
  :: (Rounding r, Precision p)
  => FilePath
  -> SDP IO (Rounded r p)
  -> IO [FilePath]
writeSDPIO xmlDir sdp = do
  createDirectoryIfMissing True xmlDir
  actions <- writeSDPActionsSimple xmlDir sdp
  traverse write actions
  where
    write (file, writeXmlFile) = do
      SDPB.unlessM (doesFileExist file) $ writeXmlFile file
      return file

withSDPSolution
  :: (Rounding r, Precision p)
  => SDPBConfig
  -> SDP IO (Rounded r p)
  -> ((SDPB.Input, SDPB.Output) -> IO a)
  -> IO a
withSDPSolution SDPBConfig{..} sdp f = do
  createDirectoryIfMissing True workDir
  withTempDirectory workDir "sdpb" $
    \baseDir -> do
      xmlFiles <- writeSDPIO (baseDir </> "xml_dir") sdp
      let input = SDPB.defaultInput (baseDir </> "sdp") xmlFiles sdpbParams
      output <- SDPB.run pvm2sdpExecutable sdpbExecutable input
      f (input, output)

