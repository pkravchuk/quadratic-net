{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE PolyKinds           #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE TypeOperators       #-}

module QuadraticNet.SDP where

import           Control.Arrow             (second)
import           Control.Monad.IO.Class    (MonadIO)
import           Control.Monad.Trans       (lift)
import           Control.Monad.Trans.Maybe (MaybeT (..), runMaybeT)
import           Data.Functor.Compose      (Compose (..))
import qualified Data.List.NonEmpty        as NE
import qualified Data.Matrix               as MU
import           Data.Matrix.Static        (Matrix)
import qualified Data.Matrix.Static        as M
import qualified Data.Text                 as T
import qualified Data.Vector               as V
import           Fmt                       ((+||), (||+))
import           GHC.TypeNats
import           Linear.Metric             (dot)
import           Numeric.Eigen.Raw         (eigenvaluesSymmetric,
                                            eigenvectorsSymmetric)
import           Numeric.Eigen.Static      (fromRawMatrix)
import           Numeric.Rounded
import           QuadraticNet.Solver       (SDPBConfig (..), defaultSDPBConfig,
                                            withSDPSolution)
import           QuadraticNet.Util         (logInfo)
import qualified SDPB.Solver               as SDPB
import qualified SDPB.Math.DampedRational  as DR
import qualified SDPB.Math.Polynomial      as Pol
import           SDPB.Math.VectorSpace     (mapTensor, transposeThroughMat)
import           SDPB.SDP.Types            (PositiveConstraint (..), SDP (..))
import qualified SDPB.Math.Util            as SDPB

data SDPRelaxationConfig = SDPRelaxationConfig
  { maxRankMinimizationSteps     :: Int
  , eigenvalueReductionThreshold :: Double
  , eigenvalueSizeThreshold      :: Double
  , sdpbConfig                   :: SDPBConfig
  } deriving (Show)

defaultSDPRelaxationConfig :: FilePath -> FilePath -> FilePath -> SDPRelaxationConfig
defaultSDPRelaxationConfig pvm2sdpExec sdpbExec workDir = SDPRelaxationConfig
  { maxRankMinimizationSteps     = 20
  , eigenvalueReductionThreshold = 0.7
  , eigenvalueSizeThreshold      = 1e-32
  , sdpbConfig                   = defaultSDPBConfig pvm2sdpExec sdpbExec workDir
  }

unitMatrixSymmetric :: (KnownNat j, Num a) => Int -> Int -> Matrix j j a
unitMatrixSymmetric i j = M.matrix $ \e ->
  if e == (i,j) || e == (j,i)
  then 1
  else 0

enumerated :: [a] -> [(T.Text, a)]
enumerated xs = [(T.pack (show @Int i), x) | (i,x) <- zip [0 ..] xs]

generateSymIndices :: Int -> (Int -> Int -> a) -> [a]
generateSymIndices dim f = do
  i <- [1 .. dim]
  j <- [1 .. i]
  return (f i j)

toConstPositiveConstraint :: (KnownNat j, Monad m, Num a, Eq a) => T.Text -> [Matrix j j a] -> PositiveConstraint m a
toConstPositiveConstraint label ms =
  PositiveConstraint label $
  pure $ DR.constant $ transposeThroughMat $ Compose $ V.fromList ms
  --pure $ BF.fromMatrixVector $ MV.concat $ map (`MV.tensor` V.singleton 1) ms
  -- let dim = fromIntegral (MU.nrows (head ms))
  -- in reifyNat dim $ \(_ :: Proxy j) ->
  --   PositiveConstraint label $
  --   let ms' = map (M.fromListUnsafe @j @j . MU.toList) ms
  --   in pure $ BF.fromMatrixVector $ MV.concat $ map (`MV.tensor` V.singleton 1) ms'

sdpRelaxation
  :: forall j a m . (KnownNat j, Eq a, Fractional a, Monad m)
  => NE.NonEmpty (Matrix j j a)
  -> SDP m a
sdpRelaxation qs = SDP
  { sdpObjective     = ("objective", pure zeroVec)
  , sdpNormalization = pure traceVec
  , sdpMatrices      = psdConstraint :
                       [ PositiveConstraint d (pure (qConstraint q))
                       | (d, q) <- enumerated (NE.toList qs)
                       ]
  , sdpSamplePoints  = const (V.singleton 0)
  , sdpDescription   = Nothing
  }
  where
    dim = M.nrows (NE.head qs)
    zeroVec  = V.fromList $ generateSymIndices dim (\_ _ -> 0)
    traceVec = V.fromList $ generateSymIndices dim (\i j -> if i == j then 1 else 0)
    psdConstraint = toConstPositiveConstraint @j "psd constraint" $
      generateSymIndices dim unitMatrixSymmetric
    qConstraint q = DR.constant . Compose . toM11 . V.fromList . generateSymIndices dim $ \i j ->
      (q M.! (i,j)) * (if i == j then 1 else 2)
    toM11 :: b -> M.Matrix 1 1 b
    toM11 a = M.matrix (const a)

rankMinimization
  :: (KnownNat j, KnownNat (j-1), Precision p, Rounding r, Monad m)
  => SDP m (Rounded r p)
  -> Matrix j j (Rounded r p)
  -> SDP m (Rounded r p)
rankMinimization sdp@SDP{..} xPsd = sdp
  { sdpObjective = second (fmap (V.cons (-1))) sdpObjective
  , sdpNormalization = fmap (V.cons 0) sdpNormalization
  , sdpMatrices =
      rankMinimizer :
      [ PositiveConstraint d (fmap (DR.mapNumerator (mapTensor (V.cons 0))) m)
      | PositiveConstraint d m <- sdpMatrices
      ]
  }
  where
    smallestEigV = smallestEigenvectors xPsd
    dim = M.nrows smallestEigV
    rankMinimizer = toConstPositiveConstraint "rank minimizer" $
      M.identity :
      generateSymIndices dim (\i j -> -M.transpose smallestEigV M..* unitMatrixSymmetric i j M..* smallestEigV)

smallestEigenvectors
  :: (KnownNat j, Precision p, Rounding r)
  => Matrix j j (Rounded r p)
  -> Matrix j (j-1) (Rounded r p)
smallestEigenvectors = M.applyUnary $
  \m -> MU.submatrix 1 (MU.nrows m) 1 (MU.ncols m - 1) (eigenvectorsSymmetric m)

solveRelaxation
  :: (KnownNat j, Rounding r, Precision p)
  => SDPBConfig
  -> SDP IO (Rounded r p)
  -> IO (Maybe (Matrix j j (Rounded r p)))
solveRelaxation sdpbConfig sdp =
  withSDPSolution sdpbConfig sdp $ \(input, output) ->
  if SDPB.isDualFeasible output
  then fmap Just $ do
    let
      getPsd (PositiveConstraint "psd constraint" getMatrix : _) = do
        Compose m' <- fmap Pol.constantTerm . DR.numerator <$> getMatrix
        --MV.MatrixVector m' <- BF.toConst <$> getMatrix
        return $ M.unpackStatic m'
      getPsd (_ : ms) = getPsd ms
      getPsd [] = error "Couldn't find psd constraint"
    m <- fromRawMatrix <$> getPsd (sdpMatrices sdp)
    norm <- sdpNormalization sdp
    y <- SDPB.readDualVector (SDPB.outDir input)
    let alpha = V.cons 1 y
    return $ fmap ((`dot` alpha) . SDPB.reshuffleWithNormalizationConst norm) m
  else return Nothing

reportEigenvalues :: (KnownNat j, Rounding r, Precision p, MonadIO m) => Matrix j j (Rounded r p) -> m ()
reportEigenvalues m =
  logInfo $ "SDP solution eigenvalues: "+||fmap toDouble (eigenvaluesSymmetric (M.unpackStatic m))||+""

minimizeSDPRank
  :: (KnownNat j, KnownNat (j-1), Rounding r, Precision p)
  => SDPRelaxationConfig
  -> SDP IO (Rounded r p)
  -> Matrix j j (Rounded r p)
  -> IO (Matrix j j (Rounded r p))
minimizeSDPRank SDPRelaxationConfig{..} sdp mPsd =
  go maxRankMinimizationSteps mPsd (maxAbsSmallEigenvalues mPsd)
  where
    reductionThresh = realToFrac eigenvalueReductionThreshold
    sizeThresh = realToFrac eigenvalueSizeThreshold
    go 0 m _ = return m
    go n m smallEvals =
      if smallEvals < sizeThresh
      then return m
      else do
        let sdp' = rankMinimization sdp m
        solveRelaxation sdpbConfig sdp' >>= \case
          Nothing -> do
            logInfo "Couldn't solve rank minimization. This shouldn't happen."
            return m
          Just m' -> do
            reportEigenvalues m'
            let smallEvals' = maxAbsSmallEigenvalues m'
            if smallEvals' < reductionThresh * smallEvals
              then go (n-1) m' smallEvals'
              else do
                logInfo "Rank minimization did not sufficiently reduce eigenvalues. Stopping."
                return (if smallEvals' < smallEvals then m' else m)

maxAbsSmallEigenvalues :: (KnownNat j, Rounding r, Precision p) => Matrix j j (Rounded r p) -> Rounded r p
maxAbsSmallEigenvalues = V.maximum . fmap abs . V.init . eigenvaluesSymmetric . M.unpackStatic

solveSDPRelaxationSmallRank
  :: (KnownNat j, KnownNat (j-1), Rounding r, Precision p)
  => SDPRelaxationConfig
  -> NE.NonEmpty (Matrix j j (Rounded r p))
  -> IO (Maybe (Matrix j j (Rounded r p)))
solveSDPRelaxationSmallRank c@SDPRelaxationConfig{..} quadraticForms =
  let sdp = sdpRelaxation quadraticForms
  in runMaybeT $ do
    m <- MaybeT $ solveRelaxation (sdpbConfig { sdpbParams = (sdpbParams sdpbConfig)
                                                { SDPB.findDualFeasible = True
                                                , SDPB.detectDualFeasibleJump = True
                                                }}) sdp
    lift $ reportEigenvalues m
    lift $ minimizeSDPRank c sdp m
