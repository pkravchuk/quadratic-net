{-# LANGUAGE DataKinds #-}
{-# LANGUAGE PolyKinds #-}

module QuadraticNet.Math where

import           Data.Foldable         (foldl')
import qualified Data.Matrix.Static    as M
import qualified Data.Vector           as V
import           GHC.TypeNats          (KnownNat)
import           Linear.Metric         (dot, norm)
import           Linear.V              (Dim, V)
import qualified Linear.V              as L
import           Linear.Vector         ((^/))
import           Numeric.Eigen.Static  (fromRawVector)
import qualified Numeric.LinearAlgebra as LA

toHMatrix :: (KnownNat j, KnownNat k, LA.Element a) => M.Matrix j k a -> LA.Matrix a
toHMatrix = LA.fromLists . M.toLists

toHVector :: (KnownNat j, LA.Element a) => V j a -> LA.Vector a
toHVector = LA.fromList . V.toList . L.toVector

fromHVector :: (KnownNat j, LA.Element a) => LA.Vector a -> V j a
fromHVector =  fromRawVector . V.fromList . LA.toList

normalizeV :: (Dim j, Floating a) => V j a -> V j a
normalizeV x = x ^/ norm x

matXvec :: (KnownNat j, KnownNat k, Num a) => M.Matrix j k a -> V k a -> V j a
matXvec m v = case M.colVector (L.toVector v) of
  Just c  -> fromRawVector $ M.getCol 1 (m M..* c)
  Nothing -> error "Matrix vector size mismatch"

bilinearPair :: (KnownNat j, Num a) => M.Matrix j j a -> V j a -> a
bilinearPair m v = v `dot` (m `matXvec` v)

bilinearPairH :: LA.Matrix Double -> LA.Vector Double -> Double
bilinearPairH m v = v `LA.dot` (m LA.#> v)

outerProduct :: (KnownNat j, KnownNat k, Num a) => V j a -> V k a -> M.Matrix j k a
outerProduct u' v' = M.matrix $ \(i, j) ->
  u V.! (i-1) * v V.! (j-1)
  where
    u = L.toVector u'
    v = L.toVector v'

sumM :: (KnownNat j, KnownNat k, Num a, Foldable f) => f (M.Matrix j k a) -> M.Matrix j k a
sumM ms = foldl' (+) M.zero ms
