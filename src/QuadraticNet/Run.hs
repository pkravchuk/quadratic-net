{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE TypeOperators     #-}
{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PolyKinds         #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE TypeApplications  #-}

module QuadraticNet.Run where

import           Control.Monad.Catch       (SomeException, try)
import           Control.Monad.Except      (ExceptT (..), liftIO, withExceptT)
import           Control.Monad.Trans.Maybe (MaybeT (..), runMaybeT)
import           Data.Foldable             (toList)
import qualified Data.List.NonEmpty        as NE
import qualified Data.Matrix.Static        as M
import           Data.Text                 (Text)
import qualified Data.Text                 as T
import           Fmt                       ((+||), (||+))
import           GHC.TypeNats
import           Linear.V                  (V)
import           Linear.Vector             ((*^))
import           Numeric.Rounded           hiding (precision)
import           QuadraticNet.Newton       (NewtonConfig (..),
                                            NewtonResult (..),
                                            defaultNewtonConfig, newtonSearch)
import           QuadraticNet.Sampling     (samplePoints)
import           QuadraticNet.SDP
import           QuadraticNet.Util         (logInfo)

data QuadraticNetConfig = QuadraticNetConfig
  { sdpRelaxationConfig :: SDPRelaxationConfig
  , newtonConfig        :: NewtonConfig
  , numSamplePoints     :: Int
  , covarianceFuzzing   :: Double
  , sdpBarrierShift     :: Double
  , precision           :: Int
  } deriving (Show)

defaultQuadraticNetConfig :: FilePath -> FilePath -> FilePath -> QuadraticNetConfig
defaultQuadraticNetConfig pvmExec sdpbExec workDir = QuadraticNetConfig
  { sdpRelaxationConfig = defaultSDPRelaxationConfig pvmExec sdpbExec workDir
  , newtonConfig        = defaultNewtonConfig
  , numSamplePoints     = 1000000
  , covarianceFuzzing   = 1e-16
  , sdpBarrierShift     = 1e-16
  , precision           = 128
  }

whenNothing :: Monad m => MaybeT m a -> m () -> MaybeT m a
whenNothing m go = MaybeT $ runMaybeT m >>= \case
  Nothing -> go >> return Nothing
  just -> return just

tryExcept :: IO a -> ExceptT Text IO a
tryExcept go = withExceptT (T.pack . show @SomeException) (ExceptT (try go))

runSearch
  :: (KnownNat j, KnownNat (j-1), Rounding r, Precision p)
  => QuadraticNetConfig
  -> NE.NonEmpty (M.Matrix j j (Rounded r p))
  -> ExceptT Text IO (Maybe (V j (Rounded r p)))
runSearch cfg@QuadraticNetConfig{..} qForms = runMaybeT $ do
  let size = M.nrows (NE.head qForms)
      qFormsShifted = fmap (\q -> q - realToFrac sdpBarrierShift *^ M.identity) qForms
  logInfo $ "Config: "+||cfg||+""
  logInfo $ "Quadratic net with "+||length qForms||+" quadratic form(s) of size "+||size||+"x"+||size||+"."
  logInfo $ "Quadratic forms: "+||map M.toLists (NE.toList qForms)||+""
  covariance <- MaybeT (tryExcept $ solveSDPRelaxationSmallRank sdpRelaxationConfig qFormsShifted)
    `whenNothing` (logInfo "No solution to sdp relaxation.")
  let c = (realToFrac covarianceFuzzing)^(2 :: Int)
      covariance' =
        (1 - c) *^ covariance + c *^ M.identity
  xFeasible <- MaybeT (tryExcept $ samplePoints qForms covariance' numSamplePoints)
    `whenNothing` (logInfo "No feasible sample points found.")
  liftIO $ case newtonSearch newtonConfig qForms xFeasible of
    MaxNewtonSteps x -> do
      logInfo "Warning: max newton steps exceeded"
      logInfo $ "Feasible point: "+||toList x||+""
      return x
    Converged steps x -> do
      logInfo $ "Newton search converged after "+||steps||+" steps."
      logInfo $ "Feasible point: "+||toList x||+""
      return x
