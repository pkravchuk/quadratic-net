{-# Language OverloadedStrings #-}

module QuadraticNet.Util where

import Control.Monad.IO.Class (liftIO, MonadIO)
import Data.Monoid ((<>))
import Data.Text (Text)
import qualified Data.Text.IO as T
import System.IO (stderr, hFlush)

logInfo :: MonadIO m => Text -> m ()
logInfo msg = liftIO $ T.hPutStrLn stderr ("[QN] " <> msg) >> hFlush stderr
